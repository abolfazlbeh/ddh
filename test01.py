data = {
    "hotels": [{"name": 'x', "rooms": [
        {"number": 113, "type": "single", "price": 100},
{"number": 114, "type": "single", "price": 100},
{"number": 115, "type": "single", "price": 300},
{"number": 116, "type": "single", "price": 50},
{"number": 117, "type": "single", "price": 200}
    ]}]
}

# query = "hotels.*.rooms.*.price"
# action = "add"
# value = 100

query = "hotels.0.rooms.4.type"
action = "update"
value = "double"



def f(d, q, action, value):
    qs = q.split('.')

    print(d, q)
    if len(qs) > 0:
        key = qs[0]

        if len(qs) > 1 :
            subkey = qs[1]

            if subkey == "*":
                if isinstance(d[key], list):
                    for item in d[key]:
                        f(item, '.'.join(qs[2:]), action, value)
                else:
                    f(d[key], '.'.join(qs[2:]), action, value)

            else:
                index = None
                try:
                    index = int(subkey)
                    if isinstance(d[key], list) and index < len(d[key]):
                        f(d[key][index], '.'.join(qs[2:]), action, value)
                except:
                    f(d[key][subkey], '.'.join(qs[2:]), action, value)

        else:
            if key in d:
                do_action(d, key, action, value)
            else:
                return


def do_action(obj, key, action, value):
    if action == "add":
        obj[key] += value
    elif action == "update":
        obj[key] = value


if __name__ == '__main__':
    f(data, query, action, value)
    print(data)